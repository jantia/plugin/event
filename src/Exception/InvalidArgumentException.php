<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Event
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Event\Exception;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface {

}
