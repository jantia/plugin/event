<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Event
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Event\Openapi\Schema\Scheme\Object;

//
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *      schema="EventTimestamp",
 *      type="object",
 *      description="Item schema",
 *       @OA\Property(
 *          property="timezone",
 *          type="string",
 *          description="Used timezone.",
 *          example="UTC"
 *      ),
 *      @OA\Property(
 *          property="date",
 *          type="string",
 *          format="date-time",
 *          description="Timestamp of the event in defined timezone.",
 *          example="2020-01-01T00:00:00"
 *      )
 * )
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class EventTimestamp {

}
