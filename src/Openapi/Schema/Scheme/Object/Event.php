<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Event
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Event\Openapi\Schema\Scheme\Object;

//
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *      schema="Event",
 *      type="object",
 *      description="Item schema",
 *      @OA\Property(
 *          property="trace",
 *          type="string",
 *          format="alphanumeric",
 *          description="A unique identifier for the trace, structured as 'Root=1-{trace}', where '{trace}' is a combination of a timestamp and a random identifier.",
 *          example="Root=1-667bb75a-9a44473846ca8349c2d6ab18"
 *      ),
 *      @OA\Property(
 *          property="uuid",
 *          type="string",
 *          description="Version 7 UUID.",
 *          example="019280fa-a50e-7103-911d-b82216541093"
 *      ),
 *      @OA\Property(
 *          property="ulid",
 *          type="string",
 *          description="ULID from previous UUID.",
 *          example="01JA0FN98EE41S27DR48B5844K"
 *      ),
 *      @OA\Property(
 *          property="status",
 *          type="integer",
 *          description="HTTP status code for whole event. Usually 201 for POST/PUT/PATCH and 200 for general success.",
 *          example=201
 *      )
 * )
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Event {

}
