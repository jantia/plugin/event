<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Event
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Event\Openapi\Schema\Scheme\Object;

//
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *       schema="EventRequest",
 *       type="object",
 *       description="Event headers response.",
 *       @OA\Property(
 *           property="uri",
 *           type="string",
 *           format="uri",
 *           description="Request full URI.",
 *           example="https://api.example.com/event/1234567890"
 *       ),
 *       @OA\Property(
 *            property="method",
 *            type="string",
 *            format="httpMethod",
 *            description="Request method.",
 *            example="GET"
 *        ),
 *       @OA\Property(
 *           property="status",
 *           type="object",
 *           description="Event query & result status.",
 *           ref="#/components/schemas/EventStatus",
 *       ),
 *       @OA\Property(
 *          property="timestamp",
 *          type="object",
 *          description="Event datetime in with timezone information.",
 *          @OA\Property(
 *             property="timezone",
 *             description="Used timezone.",
 *             ref="#/components/schemas/GlobalObjectTimestamp/properties/timezone",
 *          ),
 *          @OA\Property(
 *             property="date",
 *             type="string",
 *             description="Timestamp of the event in defined timezone.",
 *             ref="#/components/schemas/GlobalObjectTimestamp/properties/date",
 *         )
 *      )
 *   )
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class EventRequest {

}