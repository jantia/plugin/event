<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Event
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Event\Openapi\Schema\Scheme\Object;

//
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *      schema="EventStatus",
 *      type="object",
 *      description="Status codes for query & result",
 *      @OA\Property(
 *          property="query",
 *          type="object",
 *          description="Query status.",
 *          ref="#/components/schemas/EventStatusItem",
 *      ),
 *      @OA\Property(
 *          property="result",
 *          type="object",
 *          description="Result status.",
 *          ref="#/components/schemas/EventStatusItem",
 *      )
 * )
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class EventStatus {

}
