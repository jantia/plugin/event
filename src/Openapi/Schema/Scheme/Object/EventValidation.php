<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Event
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Event\Openapi\Schema\Scheme\Object;

//
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *      schema="EventValidation",
 *      type="object",
 *      description="Event validation headers.",
 *      @OA\Property(
 *          property="algo",
 *          type="string",
 *          description="Used hash algorithm.",
 *          example="sha256"
 *      ),
 *      @OA\Property(
 *          property="data",
 *          type="string",
 *          description="Hash value.",
 *          example="cf5895f14e8d3e2de63e72581f2d3f38ee57b71ae2227d0ecc0d81e5ff435b3e"
 *      ),
 *      @OA\Property(
 *          property="headers",
 *          type="string",
 *          description="Hash value.",
 *          example="64b8f3420e3637c939d5d6efec44097e09bb43fa8a1dfc3d5daaa5ff040bbab0"
 *      ),
 *      @OA\Property(
 *          property="hmac",
 *          type="string",
 *          description="HMAC value from headers + data + hash.",
 *          example="cc1de3ce5a0a8cb1c77db6e580059a5ef93f1f035a317a466a348a323e07ca9a"
 *      )
 *   )
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class EventValidation {

}