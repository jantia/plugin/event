<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Event
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Event\Openapi\Schema\Scheme\Object;

//
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *      schema="EventHashItem",
 *      type="object",
 *      description="HTTP status codes with description.",
 *      @OA\Property(
 *          property="algo",
 *          type="string",
 *          description="Name of selected hashing algorithm.",
 *          example="sha256"
 *      ),
 *     @OA\Property(
 *         property="value",
 *         type="string",
 *         description="Hash value.",
 *         example="9b7e4abdd48fd2605f9bdbb981680db3b1895"
 *     )
 *  )
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class EventHashItem {

}
