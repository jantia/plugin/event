<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Event
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Event\Openapi\Schema\Scheme\Object;

//
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *      schema="EventStatusItem",
 *      type="object",
 *      description="HTTP status codes with description.",
 *      @OA\Property(
 *          property="code",
 *          type="integer",
 *          description="HTTP status code.",
 *          example="200"
 *      ),
 *     @OA\Property(
 *         property="message",
 *         type="string",
 *         description="HTTP status code as string.",
 *         example="OK"
 *     )
 *  )
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class EventStatusItem {

}
