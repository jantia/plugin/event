<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Event
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Event\Openapi\Schema\Response\Common;

//
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *      schema="ResponseEvent",
 *      type="object",
 *      description="Event headers response.",
 *      @OA\Property(
 *          property="event",
 *          type="object",
 *          description="The unique identifier of the event.",
 *          ref="#/components/schemas/Event",
 *       ),
 *       @OA\Property(
 *           property="request",
 *           type="object",
 *           description="Details about the request.",
 *           ref="#/components/schemas/EventRequest",
 *       ),
 *       @OA\Property(
 *           property="validation",
 *           type="object",
 *           description="Details about the event validation.",
 *           nullable=true,
 *           ref="#/components/schemas/EventValidation",
 *       )
 *  )
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class ResponseEvent {

}
