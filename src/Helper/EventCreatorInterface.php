<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Event
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Event\Helper;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface EventCreatorInterface extends EventInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string EVENT_HEADER_KEY_EVENT = 'event';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string EVENT_HEADER_KEY_HASH = 'hash';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string EVENT_HEADER_KEY_REQUEST = 'request';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string EVENT_HEADER_KEY_STATUS = 'status';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string EVENT_HEADER_KEY_TIMESTAMP = 'timestamp';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string EVENT_HEADER_KEY_TRACE = 'trace';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string EVENT_HEADER_KEY_UUID = 'uuid';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string EVENT_HEADER_KEY_VALIDATION = 'validation';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const array EVENT_STATUS_KEYS = ['query', 'result'];
	
	/**
	 * Headers keys version.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	final public const string EVENT_HEADER_KEYS_VERSION = 'v1';
	
	/**
	 * Headers keys will be used in the event header and in given order (A-Z).
	 *
	 * @since   3.0.0 First time introduced.
	 */
	final public const array EVENT_HEADER_KEYS = [self::EVENT_HEADER_KEY_EVENT, self::EVENT_HEADER_KEY_REQUEST,
	                                              self::EVENT_HEADER_KEY_VALIDATION];
	
	/**
	 * Create standard headers about the event which is traceable later.
	 *
	 * @param    mixed     $content
	 * @param    array     $status
	 * @param    string    $host
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(mixed $content, array $status, string $host);
	
	/**
	 * Set the secret key for HMAC. Make sure the key is least 32 characters long.
	 *
	 * @param    string    $key
	 *
	 * @return AbstractEventCreator
	 * @since   3.0.0 First time introduced.
	 */
	public function setSecretKey(string $key) : EventCreatorInterface;
	
	/**
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getEventContent() : mixed;
	
	/**
	 * Return new UUIDv4.
	 *
	 * @return null|array
	 */
	public function getEventCode() : ?array;
	
	/**
	 * @param    mixed    $content
	 *
	 * @return EventCreatorInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setEventContent(mixed $content) : EventCreatorInterface;
	
	/**
	 * @return EventCreatorInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetEventContent() : EventCreatorInterface;
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getEventStatus() : ?array;
	
	/**
	 * @param    array    $status
	 *
	 * @return EventCreatorInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setEventStatus(array $status) : EventCreatorInterface;
	
	/**
	 * @return EventCreatorInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetEventStatus() : EventCreatorInterface;
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getEventHost() : ?string;
	
	/**
	 * @param    string    $host
	 *
	 * @return EventCreatorInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function setEventHost(string $host) : EventCreatorInterface;
	
	/**
	 * @return EventCreatorInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function resetEventHost() : EventCreatorInterface;
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function createEventHeaders() : ?array;
	
	/**
	 * @param    array    $result
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function validateResultArray(array $result) : bool;
	
	/**
	 * Return UUIDv5
	 *
	 * @param    string    $value
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getEventIdentifier(string $value) : string;
	
	/**
	 * Resolve status array & generate result array with code & message pair for each status.
	 *
	 * @param    array    $status
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function checkEventStatusResponse(array $status) : array;
	
	/**
	 * Get the keys for event status array.
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getEventStatusKeys() : array;
	
	/**
	 * Get event header keys.
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getEventHeaderKeys() : array;
	
	/**
	 * If you don't name your domain name for getEventIdentifier, use this return value as $domain var.
	 *
	 * @param    null|string    $host
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getEventHeadersHostname(?string $host) : string;
	
	/**
	 * Retrieve the validation keys for events.
	 *
	 * @return array An array of validation keys.
	 * @since   3.0.0 First time introduced.
	 */
	public function getEventValidationKeys() : array;
}
