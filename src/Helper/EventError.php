<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Event
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Event\Helper;

//
use Tiat\Stdlib\Response\ResponseStatus;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class EventError extends AbstractEventError {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const array ERROR_KEYS = ['code', 'description', 'reason', 'text', 'trace'];
	
	/**
	 * @param    array    $errors
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function createEventErrors(array $errors) : array {
		// Define fields for each error
		foreach($errors as $val):
			//
			$code = (int)( $val['code'] ?? 0 );
			
			//
			if($code >= 300 && $code <= 600):
				// Mandatory fields
				$tmp['text']  = ResponseStatus::getResponse($code);
				$tmp['code']  = $code;
				$tmp['trace'] = ID_TRACE ?? '';
				
				// Fill only if defined
				if(! empty($val['reason'])):
					$tmp['reason'] = $val['reason'];
				endif;
				
				// Fill only if defined
				if(! empty($val['description'])):
					$tmp['description'] = $val['description'];
				endif;
				
				//
				$result['error'][] = $tmp;
				unset($tmp);
			endif;
		endforeach;
		
		//
		return $result ?? [];
	}
}
