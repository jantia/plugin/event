<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Event
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Event\Helper;

//
use DateInvalidTimeZoneException;
use DateMalformedStringException;
use Jantia\Plugin\Event\Exception\DomainException;
use Jantia\Plugin\Event\Exception\RuntimeException;
use Jantia\Standard\Platform\PlatformEnv;
use Tiat\Standard\Time\TimestampInterface;

use function constant;
use function defined;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class EventCreator extends AbstractEventCreator implements TimestampInterface {
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function createEventHeaders() : ?array {
		// HAProxy Trace ID by Jantia Platform (if exists)
		if(defined(PlatformEnv::TRACE_ID->value)):
			$this->setEventHeaderKey(self::EVENT_HEADER_KEY_TRACE, constant(PlatformEnv::TRACE_ID->value));
		else:
			throw new RuntimeException("Jantia Trace ID must exists.");
		endif;
		
		// Get request URI & method (only the PATH section)
		$request = ['uri' => $this->_getHostNameWithRequest(), 'method' => ( $_SERVER['REQUEST_METHOD'] ?? NULL ),];
		
		//
		$this->setEventHeaderKey(self::EVENT_HEADER_KEY_STATUS,
		                         $this->checkEventStatusResponse($this->getEventStatus()));
		
		//
		$request[self::EVENT_HEADER_KEY_STATUS] = $this->_getEventHeaderKey(self::EVENT_HEADER_KEY_STATUS);
		
		// Unique code each time
		// status : $event->value
		$this->setEventHeaderKey(self::EVENT_HEADER_KEY_EVENT, $this->getEventCode());
		
		// Datetime in UTC format
		try {
			$this->setEventHeaderKey(self::EVENT_HEADER_KEY_TIMESTAMP,
			                         ['timezone' => 'UTC', 'date' => $this->getTimeStamp()]);
		} catch(DateMalformedStringException|DateInvalidTimeZoneException $e) {
			throw new DomainException($e->getMessage());
		}
		
		//
		$request[self::EVENT_HEADER_KEY_TIMESTAMP] = $this->_getEventHeaderKey(self::EVENT_HEADER_KEY_TIMESTAMP);
		
		// Set request URI & method
		$this->setEventHeaderKey(self::EVENT_HEADER_KEY_REQUEST, $request);
		
		// Generate hash for validation keys
		$this->setEventHeaderKey(self::EVENT_HEADER_KEY_VALIDATION, $this->getEventValidationKeys());
		
		// Return final headers
		$result = $this->_collectResultArray(TRUE);
		
		//
		if($this->validateResultArray($result) === TRUE):
			return $result;
		else:
			throw new DomainException("Event result array doesn't have valid keys.");
		endif;
	}
}
