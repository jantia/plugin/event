<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Event
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Event\Helper;

//
use Jantia\Plugin\Event\Exception\DomainException;
use Jantia\Plugin\Event\Exception\RuntimeException;
use Jantia\Stdlib\Exception\InvalidArgumentException;
use JsonException;
use Laminas\Uri\Uri;
use Ramsey\Uuid\Uuid;
use Tiat\Collection\Crypt\CryptoTrait;
use Tiat\Collection\Uri\UriHost;
use Tiat\Standard\DataModel\HttpMethod;
use Tiat\Standard\DataModel\HttpMethodType;
use Tiat\Stdlib\Identifiers\Identifiers;
use Tiat\Stdlib\Response\ResponseStatus;
use Tiat\Stdlib\Time\TimestampTrait;

use function array_is_list;
use function array_key_exists;
use function array_keys;
use function array_values;
use function hash;
use function hash_hmac;
use function implode;
use function in_array;
use function is_int;
use function json_encode;
use function ksort;
use function sprintf;
use function str_contains;
use function strlen;
use function strtolower;
use function strtoupper;
use function trim;
use function ucfirst;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractEventCreator extends AbstractEvent implements EventCreatorInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use CryptoTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use TimestampTrait;
	
	/**
	 * @var mixed
	 * @since   3.0.0 First time introduced.
	 */
	private mixed $_eventContent;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_eventStatus;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_eventHost;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_eventHostname;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_keyEvent;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_keyHash;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_keyRequest;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_keyStatus;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_keyTimestamp;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_keyTrace;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_keyUuid;
	
	/**
	 * @var array
	 * @since   3.0.0 First time introduced.
	 */
	private array $_keyValidation;
	
	/**
	 * @var string
	 * @since   3.0.0 First time introduced.
	 */
	private string $_secretKey;
	
	/**
	 * @param    mixed          $content
	 * @param    array|int      $status
	 * @param    NULL|string    $host
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(mixed $content, array|int $status, ?string $host = NULL) {
		$this->setEventContent($content)->setEventStatus($status)->setEventHost($this->getEventHeadersHostname($host));
	}
	
	/**
	 * @param    null|string    $host
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getEventHeadersHostname(?string $host) : string {
		return $host ?? $this->_getHostname();
	}
	
	/**
	 * @return string
	 * @since  3.0.0 First time introduced.
	 */
	protected function _getHostname() : string {
		//
		$uri = new Uri(( new UriHost($_SERVER) )->getUrl());
		
		//
		$tld  = ( new UriHost($_SERVER) )->getDomainTld($uri->toString());
		$name = ( new UriHost($_SERVER) )->getDomainName($uri->toString());
		
		//
		if(strtolower($tld) === 'localhost'):
			return $uri->getScheme() . '//' . $uri->getHost();
		else:
			return $uri->getScheme() . '://' . $name . '.' . $tld;
		endif;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetEventContent() : static {
		//
		unset($this->_eventContent);
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetEventStatus() : static {
		//
		unset($this->_eventStatus);
		
		//
		return $this;
	}
	
	/**
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function resetEventHost() : static {
		//
		unset($this->_eventHost);
		
		//
		return $this;
	}
	
	/**
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function getEventCode() : array {
		// Create a UUIDv7 at first (with timestamp)
		$uuid = ( $i = new Identifiers() )->createUuidV7();
		$ulid = $i->createUlid($uuid);
		
		// Get the result
		$request =
			strtoupper($this->_getEventHeaderKey(self::EVENT_HEADER_KEY_REQUEST)['method'] ?? HttpMethod::GET->value);
		$query   = $this->_getEventHeaderKey(self::EVENT_HEADER_KEY_STATUS)['query']['code'] ?? 0;
		$result  = $this->_getEventHeaderKey(self::EVENT_HEADER_KEY_STATUS)['result']['code'] ?? 0;
		
		// Convert cases to string
		foreach(HttpMethodType::toMethod(HttpMethodType::MODIFIES_RESOURCES) as $val):
			$cases[] = $val->value;
		endforeach;
		
		// Set EITHER 200 or 201 at first
		if(in_array($request, $cases, TRUE) === TRUE):
			$code = 201;
		else:
			$code = 200;
		endif;
		
		// Compare code to result code
		$status = max($code, $query, $result);
		
		// Create
		return ['trace' => $this->_getEventHeaderKey(self::EVENT_HEADER_KEY_TRACE), 'uuid' => $uuid->toString(),
		        'ulid' => $ulid, 'status' => $status];
	}
	
	/**
	 * @param    string    $key
	 *
	 * @return string
	 * @since  3.0.0 First time introduced.
	 */
	protected function _getEventHeaderKey(string $key) : mixed {
		//
		$name = $this->_generateEventHeaderKeyName($key);
		
		//
		return $this->{$name} ?? NULL;
	}
	
	/**
	 * @param    string    $key
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _generateEventHeaderKeyName(string $key) : string {
		return sprintf('_key%s', ucfirst(strtolower($key)));
	}
	
	/**
	 * @param    string    $key
	 * @param    string    $value
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setEventHeaderKey(string $key, mixed $value) : static {
		//
		$name          = $this->_generateEventHeaderKeyName($key);
		$this->{$name} = $value;
		
		//
		return $this;
	}
	
	/**
	 * @param    array    $status
	 *
	 * @return array[]
	 * @since   3.0.0 First time introduced.
	 */
	public function checkEventStatusResponse(array $status) : array {
		//
		[$key1, $key2] = $this->getEventStatusKeys();
		
		//
		$status = $this->_checkStatusArray($status);
		
		// IF both keys are missing or value is zero then throw an error.
		if((int)( $status[$key1] ?? 0 ) === 0 || (int)( $status[$key2] ?? 0 ) === 0):
			$msg = sprintf("Query (got: %u) & result (got %u) keys must have valid HTTP status code",
			               (int)( $status[$key1] ?? 0 ), (int)( $status[$key2] ?? 0 ));
			throw new InvalidArgumentException($msg);
		else:
			return [$key1 => ['code' => (int)( $status[$key1] ?? 0 ),
			                  'message' => ResponseStatus::getResponse($status[$key1] ?? 0)],
			        $key2 => ['code' => (int)( $status[$key2] ?? 0 ),
			                  'message' => ResponseStatus::getResponse($status[$key2] ?? 0)]];
		endif;
	}
	
	/**
	 * @return array|string[]
	 * @since   3.0.0 First time introduced.
	 */
	final public function getEventStatusKeys() : array {
		return self::EVENT_STATUS_KEYS;
	}
	
	/**
	 * Validates and standardizes the input status, which can be an integer or an array.
	 * Ensures the status is a valid HTTP status code or an array with specific keys having valid HTTP status codes.
	 *
	 * @param    array|int    $status    The input status which can be an integer or an array
	 *
	 * @return array An array of status codes with required keys and valid HTTP status codes
	 * @since   3.0.0 First time introduced.
	 */
	private function _checkStatusArray(array|int $status) : array {
		//['query'=>200, 'result'=>403]
		// Get keys
		[$key1, $key2] = $this->getEventStatusKeys();
		
		// Convert single int to array
		if(is_int($status)):
			$status = [$key1 => $status, $key2 => $status];
		// Check if keys are list
		elseif(array_is_list($status)):
			// If there is only one key then convert it to array with same status.
			if(count($status) === 1):
				$status = [$key1 => $status[0], $key2 => $status[0]];
			else:
				// There are two keys so add missing key for each status.
				$status = [$key1 => $status[0], $key2 => $status[1]];
			endif;
		else:
			// Array has keys so check that those exist and are valid.
			// Replicate the value from key which exits.
			if(array_key_exists($key1, $status) === FALSE && array_key_exists($key2, $status) === TRUE):
				$status[$key1] = $status[$key2];
			endif;
			
			if(array_key_exists($key2, $status) === FALSE && array_key_exists($key1, $status) === TRUE):
				$status[$key2] = $status[$key1];
			endif;
		endif;
		
		// Check that all keys have value & values are valid HTTP status codes.
		$values = array_values($status);
		foreach($status as &$value):
			// Convert value to integer.
			if(! is_int($value)):
				// If value is numeric string then convert it to integer.
				if(is_numeric($value = trim($value))):
					$value = (int)$value;
				endif;
				
				// Check if value is valid HTTP status code.
				if($value < 100 || $value >= 600):
					$msg =
						sprintf("Event status array must have keys: %s and values must be valid HTTP codes. Got keys %s with values %s.",
						        implode(',', self::EVENT_STATUS_KEYS), implode(array_keys($status)),
						        implode(', ', $values));
					throw new InvalidArgumentException($msg);
				endif;
			endif;
		endforeach;
		
		// Return validated status array.
		return $status;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getEventStatus() : ?array {
		return $this->_eventStatus ?? NULL;
	}
	
	/**
	 * @param    array|int    $status
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setEventStatus(array|int $status) : static {
		// Check that status array is valid
		$this->_eventStatus = $this->_checkStatusArray($status);
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $value
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getEventIdentifier(string $value) : string {
		// Generate identifier with Full URI
		return Uuid::uuid5(Uuid::uuid5(Uuid::NAMESPACE_URL, $this->_getEventHostname()), $value)->toString();
	}
	
	/**
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getEventHostname() : string {
		//
		if(empty($this->_eventHostname)):
			$this->_eventHostname = $this->_detectHostname();
		endif;
		
		//
		return $this->_eventHostname;
	}
	
	/**
	 * @param    string    $prefix
	 *
	 * @return string
	 * @since  3.0.0 First time introduced.
	 */
	protected function _detectHostname(string $prefix = 'HTTP_X_') : string {
		// Try to detect original host
		if(! empty($host = $_SERVER[$prefix . 'FORWARDED_HOST'] ?? $_SERVER['HTTP_HOST'] ?? NULL)):
			// Use X-Forwarded-For if available, PROTO might have multiple values
			if(! empty($proto = $_SERVER['HTTP_X_FORWARDED_PROTO'] ?? $_SERVER['HTTP_X_SCHEME'])):
				if(str_contains($proto, UriHost::SCHEME_HTTPS)):
					$scheme = UriHost::SCHEME_HTTPS;
				elseif(str_contains($proto, UriHost::SCHEME_HTTP)):
					$scheme = UriHost::SCHEME_HTTP;
				endif;
			endif;
			
			// Resolve scheme if not provided
			if(empty($scheme)):
				// Use URI interface to get the original host
				$uri    = new Uri(( new UriHost($_SERVER) )->getUrl());
				$scheme = $uri->getScheme();
			endif;
			
			// Try to detect original host
			return $scheme . '://' . $host;
		else:
			return $this->_getHostname();
		endif;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	public function getEventHost() : ?string {
		return $this->_eventHost ?? NULL;
	}
	
	/**
	 * @param    string    $host
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setEventHost(string $host) : static {
		//
		$this->_eventHost = $host;
		
		//
		return $this;
	}
	
	/**
	 * @param    array    $result
	 *
	 * @return bool
	 * @since   3.0.0 First time introduced.
	 */
	public function validateResultArray(array $result) : bool {
		//
		if(! empty($result)):
			//
			$keys = array_keys($result);
			foreach($keys as $val):
				if(! in_array($val, self::EVENT_HEADER_KEYS, TRUE)):
					return FALSE;
				endif;
			endforeach;
			
			// All validated
			return TRUE;
		endif;
		
		//
		return FALSE;
	}
	
	/**
	 * @return array|string[]
	 * @since   3.0.0 First time introduced.
	 */
	final public function getEventHeaderKeys() : array {
		return self::EVENT_HEADER_KEYS;
	}
	
	/**
	 * @return array
	 */
	public function getEventValidationKeys() : array {
		// Get Event Content hash
		$data = $this->getEventContent();
		
		// Important so that everything can be validated later
		// Calculate the UUID and HASH from headers
		try {
			// Collect HEADERS and create unique identifier
			$headers = hash($this->getHash(), json_encode($this->_collectResultArray(FALSE), JSON_THROW_ON_ERROR));
			
			// Generate message for HMAC
			$message = $headers . $data . $this->getHash();
			
			// Generate HMAC
			$hmac = hash_hmac($this->getHash(), $message,
			                  $this->_getSecretKey() ?? throw new RuntimeException('No secret key provided.'));
		} catch(JsonException|RuntimeException $e) {
			throw new InvalidArgumentException($e->getMessage());
		}
		
		//
		$result = ['headers' => $headers, 'data' => $data, 'algo' => $this->getHash(), 'hmac' => $hmac];
		ksort($result, SORT_STRING);
		
		//
		return $result;
	}
	
	/**
	 * @return null|array
	 * @since   3.0.0 First time introduced.
	 */
	public function getEventContent() : mixed {
		return $this->_eventContent ?? NULL;
	}
	
	/**
	 * @param    mixed    $content
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function setEventContent(mixed $content) : static {
		// Event content can be anything or in any format.
		// We DO NOT want to save it as it for security reasons.
		// So if content is decrypted or encrypted, do not save it.
		// Just make the HASH from it and save the HAS ITSELF as the content.
		
		// If content is scalar then stringify it with SHA-256 directly.
		// Otherwise, use serialize it.
		if(is_scalar($content)):
			$hash = $this->hashData((string)$content);
		else:
			$hash = $this->hashData(serialize($content));
		endif;
		
		// Discard the original content
		unset($content);
		
		// Save the hashed content for future reference.
		$this->_eventContent = $hash;
		
		//
		return $this;
	}
	
	/**
	 * @param    bool    $validation    If FALSE then don't include VALIDATION section in result array.
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _collectResultArray(bool $validation = FALSE) : array {
		//
		foreach(self::EVENT_HEADER_KEYS as $val):
			if($validation === FALSE && $val === self::EVENT_HEADER_KEY_VALIDATION):
				continue;
			else:
				$result[$val] = $this->getEventHeaderKey($val);
			endif;
		endforeach;
		
		// Sort keys in ascending order.
		if(! empty($result)):
			ksort($result);
		else:
			throw new DomainException("Event result array doesn't have valid keys.");
		endif;
		
		//
		return $result;
	}
	
	/**
	 * @param    string    $key
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getEventHeaderKey(string $key) : mixed {
		//
		$name = $this->_generateEventHeaderKeyName($key);
		
		//
		return $this->{$name} ?? NULL;
	}
	
	/**
	 * @return null|string
	 * @since   3.0.0 First time introduced.
	 */
	final protected function _getSecretKey() : ?string {
		return $this->_secretKey ?? NULL;
	}
	
	/**
	 * Set the secret key for HMAC. Make sure the key is least 32 characters long.
	 *
	 * @param    string    $key
	 *
	 * @return AbstractEventCreator
	 * @since   3.0.0 First time introduced.
	 */
	final public function setSecretKey(string $key) : static {
		if(( $length = strlen($key = trim($key)) ) >= 32):
			$this->_secretKey = $key;
		else:
			$msg = sprintf('Secret key must be at least 32 characters long. Given: %u', $length);
			throw new InvalidArgumentException($msg);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return string
	 */
	protected function _getHostNameWithRequest() : string {
		return $this->_detectHostname() . $_SERVER['REQUEST_URI'];
	}
}
