<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Event
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Event\Helper;

//
use Tiat\Standard\DataModel\InterfaceEnum;
use Tiat\Standard\DataModel\InterfaceEnumString;
use Tiat\Standard\DataModel\TraitEnum;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
enum EventStatus: string implements InterfaceEnum, InterfaceEnumString {
	
	//
	use TraitEnum;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case ERROR = 'ERROR';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case FAILED = 'FAILED';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case FATAL = 'FATAL';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case INVALID = 'INVALID';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case INITIALIZED = 'INITIALIZED';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	case OK = 'OK';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const array COMMON_EVENT_ERRORS = [self::ERROR, self::FAILED, self::FATAL, self::INVALID];
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const array GENERAL_EVENT_CODES = [self::INITIALIZED, self::OK];
	
	/**
	 * @param    EventStatus    $code
	 *
	 * @return string
	 * @since   3.0.0 First time introduced.
	 */
	public function getReason(EventStatus $code) : string {
		return match ( $code ) {
			self::ERROR => 'General error state',
			self::FAILED => 'Event failed',
			self::FATAL => 'For various reasons, the batch containing this event failed settlement',
			self::INVALID => 'Invalid format or field',
			self::INITIALIZED => 'Event has been initialized and nothing have done yet. This is first phase for new event.',
			self::OK => 'Ok'
		};
	}
}
