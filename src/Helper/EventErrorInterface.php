<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Plugin/Event
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Plugin\Event\Helper;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface EventErrorInterface extends EventInterface {
	
	/**
	 * Create standard error fields.
	 *
	 * @param    array    $errors
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	public function createEventErrors(array $errors) : array;
}
